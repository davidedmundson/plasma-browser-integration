
DEFAULT_EXTENSION_SETTINGS = {
    mpris: {
        enabled: true
    },
    kdeconnect: {
        enabled: true
    },
    downloads: {
        enabled: true
    },
    tabsrunner: {
        enabled: true
    },
    slc: {
        enabled: true
    },
    incognito: {
        // somewhat buggy, only works when allow in incognito is on
        // and I'm actually tempted to remove this
        enabled: false
    },
    breezeScrollBars: {
        // this breaks pages in interesting ways, disable by default
        enabled: false
    }
};

IS_FIREFOX = (typeof InstallTrigger !== "undefined"); // heh.
